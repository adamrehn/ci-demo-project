#!/usr/bin/env python3

class MyClass:
	
	def __init__(self, number):
		self._number = number
	
	def doubleNum(self):
		return self._number * 2

if __name__ == '__main__':
	
	instance = MyClass(1)
	expected = 2
	result = instance.doubleNum()
	if result != expected:
		raise RuntimeError('Expected {}, got {}!'.format(expected, result))
	
	print('All unit tests passed!')
